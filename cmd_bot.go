//набор функций для команд из бота
package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"syscall"
	"time"
	"unsafe"

	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/v3/mem"
	"github.com/xlab/closer"
	"golang.org/x/sys/windows/registry"
	"main.go/telegramm"
)

//тип пользователь для SkorPay, наследует пользователя телеграмм
type User_SkorPay struct {
	User_bot     *telegramm.User_bot `json:"User_bot"`
	Current_Card string              `json:"Current_Card"` // последняя выбранная карта
}

type type_Users_SkorPay map[string]*User_SkorPay

var Users_SkorPay = type_Users_SkorPay{} //Все пользователи бота

//соответствие названия команды и функции
var Name_to_func = map[string]func(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int){
	"Балансы":             balances,
	"balans":              balans,
	"О карте":             about_card,
	"О сервере":           about_server,
	"Отмена перезагрузки": cansel_reset,
	"Reboot":              reboot_server,
	"Reboot now":          reset_server_now,
	"Статусы команд":      status_cmd,
	"Данные BIN банков":   get_BIN,
	"Пользователи":        users,
	"Должники":            otchet_dolgov,
	"Прочее":              empty_func,
	"Смена ПС":            empty_func,
	"Return":              empty_func,
	"О чат боте":          about_bot,
	"Up":                  Up_Down,
	"Down":                Up_Down,
	"Start":               Start_Stop,
	"Stop":                Start_Stop,
	"Refill":              Refill,
	"Обновить программу":  update,
	"Переустановить RDP":  run_cmd,
	"Перезапустить программу": reboot_bot,
	"Завершить программу":     exit,
	"GracePeriod":             GracePeriod,
	"Все команды бота":        list_cmd_bot,
	"Уведомления":             notification,
	"Настройки бота":          change_reestr,
}

var list_card []string // список карт
// ветки реестра CURRENT_USER + SkorPay\, SkorPay\\bot_cmd, SkorPay\\bot_cmd\\end
var Keys_reestr = map[string]registry.Key{"": 0, "\\bot_cmd": 0, "\\bot_cmd\\end": 0}

/*
//Исправление ошибки MarshalJSON
// Type_user_ID сохранялась как строка
func (value type_Users_SkorPay) MarshalJSON() ([]byte, error) {
	json, err := json.Marshal(value)
	log.Println(json, err)
	return json, err
}
*/

//Запись пользователя в БД
func (user *User_SkorPay) Save_to_db() {
	//save_data_to_file_json("Users_SkorPay.json", Users_SkorPay)
}

//Добополнение в map Name_to_func команд с переменным именем
func init_cmd_bot() {
	//func init() {

	//заполняем параметры из config.ini[SkorPay]
	telegramm.Load_all_params_from_ini("SkorPay", "path_key_reestr,format_str_table_users,sec_berfor_reset", "")
	telegramm.Load_all_params_from_ini("SkorPay", "sec_await_answer", 0)

	//заполняем Keys_reestr[""]
	var err error
	for path_key := range Keys_reestr {
		path_reestr := telegramm.Param_str["path_key_reestr"] + path_key
		Keys_reestr[path_key], err = registry.OpenKey(registry.CURRENT_USER, path_reestr, registry.ALL_ACCESS)
		CheckFatallError(err, "Ошибка открытия ветки реестра", "registry.CURRENT_USER", path_reestr)
	}

	//Обновление telegramm.Cmd_name
	for _, сard := range strings.Split(get_string_from_reestr("commonwork_karts"), ";") {
		telegramm.Cmd_name["/"+сard] = сard
		Name_to_func[сard] = select_card // соответсвие названия и функции команды
	}

	//Обновление telegramm.Cmd_name
	for _, key_cmd := range []string{"Up", "Down", "Start", "Stop", "balans", "Refill", "GracePeriod"} {
		telegramm.Cmd_name["/"+key_cmd] = key_cmd
	}

	// Обновление Name_to_func всеми командами из группы change_ps
	for _, menu_item := range telegramm.Menu_bot.Childs["Основные"].Childs["Смена ПС"].Childs {
		if menu_item.Name != "Return" {
			telegramm.Cmd_name[menu_item.Cmd] = menu_item.Name
			Name_to_func[menu_item.Name] = change_ps
		}
	}

	// обработка change_bit_*
	for num_bit := 0; num_bit < 16; num_bit++ {
		cmd := fmt.Sprintf("change_bit_%d", num_bit)
		telegramm.Cmd_name["/"+cmd] = cmd
		Name_to_func[cmd] = change_bit_check_box
	}

	list_card = strings.Split(get_string_from_reestr("commonwork_karts"), ";")

	//Загрузка Users_SkorPay
	//load_data_from_file_json("Users_SkorPay.json", Users_SkorPay)
}

func cmd_bot_Close() {
	log.Println("cmd_bot Close:", Keys_reestr[""].Close())
}

//изменение значений в реестрое
func change_reestr(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	var key string
	var err error
	// формируем строку о возможных изменений
	telegramm.Param_str["commonwork_karts"] = get_string_from_reestr("commonwork_karts")
	list_key_resstr := strings.Split("commonwork_karts,"+telegramm.Param_str["keys_in_env"], ",")
	text_msg := "/change_reestr Выберитет ключ меняемого значения:\n\n"
	for _, key = range list_key_resstr {
		text_msg += "/" + key + " - " + telegramm.Param_str[key] + "\n\n"
	}

	//получение ключа-значения от пользователя
	key = telegramm.Input_string(user_SkorPay.User_bot, text_msg)

	//удаляем первый символ, если это /
	if key[0:1] == "/" {
		key = key[1:]
	}

	//проверка значения
	if !telegramm.Arr_string_contains(list_key_resstr, key) {
		telegramm.Bot_send_message(user_SkorPay.User_bot.ID, 0, "Знчение должно быть из списка: "+
			strings.Join(list_key_resstr, ""), 0)
		return
	}

	//получение нового значения от пользователя
	new_value := telegramm.Input_string(user_SkorPay.User_bot, "Новое значение для "+key)

	if new_value != "" {
		if strings.Contains(telegramm.Param_str["keys_in_env"], key) {
			// значение в переменных окружениях
			err = os.Setenv(key, new_value)
		} else {
			err = Keys_reestr[""].SetStringValue(key, new_value)
		}
		if err == nil {
			telegramm.Bot_send_message(user_SkorPay.User_bot.ID, 0, "Успешно установлено новое значение\n/reboot_bot - Перезапустить бот", 0)
			telegramm.Param_str[key] = new_value
			return
		}

		telegramm.Bot_send_message(user_SkorPay.User_bot.ID, 0, "Ошибка сохранения нового значение "+fmt.Sprint(err), 0)
	}

}
func select_card(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	user_SkorPay.Current_Card = *name_cmd
	balances(user_SkorPay, nil, nil, MessageID)
	user_SkorPay.Save_to_db()
}

//запуск cmd
func run_cmd(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	cmd_Params := telegramm.Menu_bot.Childs["Основные"].Childs["Прочее"].Childs[*name_cmd].Params
	telegramm.Bot_send_message_admins("Запуск " + cmd_Params)
	err := exec.Command("cmd.exe", "/C", "start", "/Wait", "/low", cmd_Params).Run()
	log.Println("err запуска", cmd_Params, err)
	if err == nil {
		telegramm.Bot_send_message_admins("Ошибка запуска " + cmd_Params + " :" + err.Error())
	}
	telegramm.Bot_send_message_admins("Успешное завершение работы " + cmd_Params)
}

func Start_Stop(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	if user_SkorPay.Current_Card != "" {
		var value_path string
		if *name_cmd == "Stop" {
			value_path = "2099"
		}
		Check_err(Keys_reestr[""].SetStringValue(user_SkorPay.Current_Card+"_Использовать_с", value_path))
	}
	balances(user_SkorPay, nil, nil, MessageID)
}

//Обновление программы
func update(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	telegramm.Update_programm(user_SkorPay.User_bot.ID)
}

//Завершение работы приложения, при поторной команде, будет отмена
func exit(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	if telegramm.Arr_string_contains(telegramm.Param_strings["admin_users_list"], fmt.Sprint(user_SkorPay.User_bot.ID)) {
		_, ok := telegramm.Param_int["Завершение работы"]
		if !ok {
			telegramm.Param_int["Завершение работы"] = 0
		}
		telegramm.Param_int["Завершение работы"]++

		time_wait := time.Minute
		text := "Завершение работы программы через " + time_wait.String() + " от @" + user_SkorPay.User_bot.Nikname + " отмена: /exit"
		if telegramm.Param_int["Завершение работы"] == 1 {
			telegramm.Bot_send_message_admins(text)
		} else {
			telegramm.Bot_send_message_admins("Завершение отменено @" + user_SkorPay.User_bot.Nikname)
			telegramm.Param_int["Завершение работы"] = 0
			return
		}

		//пауза и завершаем работу
		time.Sleep(time_wait)
		telegramm.Bot_send_message_admins("Завершение работы")
		if telegramm.Param_int["Завершение работы"] == 1 {
			log.Println(text)
			closer.Close()
		}
	} else {
		telegramm.Bot_send_message(user_SkorPay.User_bot.ID, 0, "Не достаточно прав для команды /"+*cmd, MessageID)
	}
}

func balans(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	send_cmd_to_SkorPay_and_await_answer(user_SkorPay.User_bot, "balans_"+user_SkorPay.Current_Card, cmd, MessageID)
}

//Пополнение карты
func Refill(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	//проверка карты
	if user_SkorPay.Current_Card == "" {
		telegramm.Bot_send_message(user_SkorPay.User_bot.ID, nil, "Выберите карту", 0)
		return
	}

	//ввод суммы пополнения
	summa := telegramm.Input_summa(user_SkorPay.User_bot, "Введите сумму пополнения карты "+user_SkorPay.Current_Card)
	if summa == "" {
		return
	}

	//отпрпавка коамнды пополнения в SkorPay
	send_cmd_to_SkorPay_and_await_answer(user_SkorPay.User_bot, "refill_"+user_SkorPay.Current_Card+"_"+summa, cmd, MessageID)
}

//Отправка команды в PayPro, ожидание ответа, текст ответа возвращается функцией
func send_cmd_to_SkorPay_and_await_answer(User_bot *telegramm.User_bot, cmd_to_SkorPay string, cmd_bot *string, MessageID int) *string {
	// Номер команды в hex
	number_cmd_hex := Send_cmd_to_SkorPay(&cmd_to_SkorPay)

	//команда строкой
	cmd_to_SkorPay += "# from @" + User_bot.Nikname + " " + number_cmd_hex

	//запуск ожидания ответа
	return await_answer_from_SkorPay(User_bot, number_cmd_hex, &cmd_to_SkorPay, cmd_bot, MessageID)
}

//ожидание ответа от SkorPay, текст ответа в chat_ID и возвращается функцией
func await_answer_from_SkorPay(User_bot *telegramm.User_bot, number_cmd_hex string, cmd_to_SkorPay, cmd_bot *string, MessageID int) *string {
	var err error
	var answer string
	telegramm.Bot_send_message(User_bot.ID, User_bot, fmt.Sprintf("Ожидание ответа команды %s %s %d секунд", *cmd_bot, *cmd_to_SkorPay, telegramm.Param_int["sec_await_answer"]), 0)
	for num_check := 0; num_check < telegramm.Param_int["sec_await_answer"]; num_check++ {
		time.Sleep(time.Second * 2)
		answer, _, err = Keys_reestr["\\bot_cmd\\end"].GetStringValue(number_cmd_hex + "_end")
		if err != nil {
			// команда еще не исполнена
			continue
		}
		break
	}

	if answer == "" {
		answer = "Нет ответа"
	}

	//telegramm.Bot_send_message(chat_ID, User_bot, "Ответа команды /"+*cmd_bot+": "+answer, MessageID)
	telegramm.Bot_send_message(User_bot.ID, User_bot, answer, MessageID)
	return &answer
}

// сохранение команды в реестр и возвращает сохраненной номер команды для PayPro
func Send_cmd_to_SkorPay(cmd_to_SkorPay *string) (number_cmd_hex string) {
	number_cmd, _, _ := Keys_reestr["\\bot_cmd"].GetIntegerValue("index")
	number_cmd++
	Keys_reestr["\\bot_cmd"].SetQWordValue("index", number_cmd)
	number_cmd_hex = fmt.Sprintf("%04x", number_cmd)
	Keys_reestr["\\bot_cmd"].SetStringValue(number_cmd_hex, *cmd_to_SkorPay) //номер в 4 символа hex, в питоне '0>4x'
	return
}

//Выводит список всех команд, для регистрации в BotFather
//Длина одного сообщения в личных чатах, группах и каналах — до 4096 символов.
func list_cmd_bot(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	all_cmd := "/list_cmd_bot Список команд для регистрирации в боте @BotFather:\n"
	for cmd, name := range telegramm.Cmd_name {
		all_cmd += fmt.Sprintf("%s - %s\n", strings.ToLower(cmd[1:]), name)
	}
	log.Println(all_cmd)
	telegramm.Bot_send_message(user_SkorPay.User_bot.ID, user_SkorPay.User_bot, all_cmd, 0)
}

func GracePeriod(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	send_cmd_to_SkorPay_and_await_answer(user_SkorPay.User_bot, "GracePeriod_"+user_SkorPay.Current_Card, cmd, MessageID)
}

func empty_func(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	telegramm.Bot_send_message(user_SkorPay.User_bot.ID, user_SkorPay.User_bot, *cmd, 0)
}

func reboot_bot(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	telegramm.Reboot_bot(user_SkorPay.User_bot.ID)
}

func balances(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	textInfo := "Карта|Баланс|Платежей|Использовать с|Последняя ошибка\n"

	for _, сard := range strings.Split(get_string_from_reestr("commonwork_karts"), ";") {
		infoСard := get_info_about_card(сard)
		var tagB1, tagB2 string
		//if infoСard["_КолПлатежейВДень"] != "" {
		//	tagB1, tagB2 = ` <b>`, `</b>`
		//}

		strInfo := "\n" + `/` + сard + " " + tagB1 + infoСard["_Баланс"] + `/` + infoСard["_КолПлатежейВДень"] + `/` + infoСard["_Использовать_с"] + tagB2 + `/` + infoСard["_last_error_screenshot"]

		if сard == user_SkorPay.Current_Card { // текущую карту выделяем наклоном и подчеркиванием
			strInfo = `<b><u><i>` + strInfo + `</i></u> /Up▲ /Down▼  /Stop⛔  /Start🚀 /balans /Refill /GracePeriod</b>`
		}
		textInfo += strInfo
		/*
		 */
	}
	telegramm.Bot_send_message(user_SkorPay.User_bot.ID, nil, textInfo, 0)
}

//Текущую карту поднимаем вверх/вниз
func Up_Down(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	if user_SkorPay.Current_Card != "" {
		index_card := find_string(user_SkorPay.Current_Card, list_card)
		if *name_cmd == "Up" && index_card > 0 {
			list_card[index_card-1], list_card[index_card] = list_card[index_card], list_card[index_card-1]
		} else if index_card < len(list_card)-1 {
			list_card[index_card+1], list_card[index_card] = list_card[index_card], list_card[index_card+1]
		}
		CheckFatallError(Keys_reestr[""].SetStringValue("commonwork_karts", strings.Join(list_card, ";")))
	}
	balances(user_SkorPay, nil, nil, MessageID)
}

func change_ps(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	//определяем команду для SkorPay
	cmd_SkorPay := telegramm.Menu_bot.Childs["Основные"].Childs["Смена ПС"].Childs[*name_cmd].Params

	send_cmd_to_SkorPay_and_await_answer(user_SkorPay.User_bot, cmd_SkorPay, cmd, MessageID)
}

// Отпавляем в чат описание о текущей карте
func about_card(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	/*
	       # Лимит {Calc_summa_limit(karta)}\n
	       for group in dict_info_card["Groups_name"]:
	           info += f'\n{group}: {dict_info_card.get("operators_"+group, "")}'
	       if karta[0:2] in ['ti', 'sb']:
	           info += '\nШаблоны: /add\n'
	       else:
	           info += '\nШаблоны не поддерживаются\n'
	       templates_pays = sm.load_dict_from_file(karta) #.get("templates_pays", {})
	       for item_template in templates_pays:
	           info += f'/{item_template} {templates_pays[item_template]} /del_{item_template} /edit_{item_template}\n'
	       info += 'Пополнить счет: /refill /refill_6000\n Сумма в льготный период: /GracePeriod'

	       # for name_group in dict_info_card["Groups_name"]:
	       #     info += f'Операторы {name_group}: {dict_info_card["Operators_" + name_group]}\n'

	       return info
	   else:
	       return 'Карта не выбрана'

	*/
	if user_SkorPay.Current_Card == "" {
		telegramm.Bot_send_message(user_SkorPay.User_bot.ID, nil, "Выберите карту", 0)
		return
	}

	dict_info_card := get_info_about_card(user_SkorPay.Current_Card)
	info := fmt.Sprintf("/%s\nБаланс %s от %s\nИспользовать с: %s\nПлатежей: %s (до %s)\nКол-во пополнений карты за день: %s/%s\nСумма пополнений карты за день: %s\nМинимальная сумма %s\nScreenshot последней ошибки: %s",
		user_SkorPay.Current_Card, dict_info_card["_Баланс"], dict_info_card["_Баланс_Дата"],
		dict_info_card["_Использовать_с"], dict_info_card["_КолПлатежейВДень"], dict_info_card["_MaxCountPayInDay"],
		dict_info_card["_CountRefillInDay"], dict_info_card["_MaxCountRefillInDay"], dict_info_card["_SummaRefillInDay"],
		dict_info_card["_min_summa"], dict_info_card["_last_error_screenshot"])

	telegramm.Bot_send_message(user_SkorPay.User_bot.ID, nil, info, 0)
}

//Перезагрузка сервера, после получения ответа остановки ПС SkorPay
func reboot_server(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	if *send_cmd_to_SkorPay_and_await_answer(user_SkorPay.User_bot, "reboot_server", cmd, MessageID) == "+" {
		//Пришел успешный ответ об остановке SkorPay, перегружаемся
		reset_server_now(user_SkorPay, name_cmd, cmd, MessageID)
	}
}

//Перезагрузка сервера немедленно через запуск shutdown.exe
//Отправка всем админам сообщения с командой для отмены /cansel_reset
func reset_server_now(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	cmd_arg_comment := fmt.Sprintf("\"Перезагрузка от @%s\" через %s секунд", user_SkorPay.User_bot.Nikname, telegramm.Param_str["sec_berfor_reset"])
	err := exec.Command("shutdown.exe", "/t ", telegramm.Param_str["sec_berfor_reset"], "/r", "/c", cmd_arg_comment).Run()
	if err != nil {
		//При ошибке уведомляем только автора перезагруки
		telegramm.Bot_send_message(user_SkorPay.User_bot.ID, nil, err.Error(), 0)
	} else {
		telegramm.Bot_send_message_admins(cmd_arg_comment + " Отмена: /cansel_reset")
	}
}

//отмена перезагрузки, через команду //shutdown /a
func cansel_reset(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {

	telegramm.Bot_send_message_admins("Отмена перезагрузки /cansel_reset от @" + user_SkorPay.User_bot.Nikname)

	err := exec.Command("shutdown.exe", "/a").Run()
	if err != nil {
		telegramm.Bot_send_message_admins("Ошибка /cansel_reset: " + err.Error())
	}
}

func status_cmd(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
}

func get_BIN(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	send_cmd_to_SkorPay_and_await_answer(user_SkorPay.User_bot, "Get_BIN", cmd, MessageID)
}

func users(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	text := fmt.Sprintf("/users Пользователи чат бота %s\n<pre>", telegramm.Bot.Self.UserName)

	//создаем форматную строку для таблицы пользователей
	formar_str := telegramm.Param_str["format_str_table_users"] + "\n"

	text += fmt.Sprintf(strings.Replace(formar_str, "d", "s", -1), "ID", "NickName", "Name", "Права", "Время последненго доступа")

	for _, user := range telegramm.Users_bot {
		text += fmt.Sprintf(formar_str, user.ID, user.Nikname, user.Name, user.Rights, user.LastTime.Format("02.01.2006 15.04.05 MST"))
	}
	text += "</pre>"
	telegramm.Bot_send_message(user_SkorPay.User_bot.ID, nil, text, 0)
}

// Системная информация о сервере
func about_server(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	info, _ := host.Info()
	text := fmt.Sprintln("/about_server\n", info.Hostname, info.Platform)

	//Читаем свободное место на HDD
	text += "Свободное место на дисках: "
	var freeBytes int64
	kernel32 := syscall.MustLoadDLL("kernel32.dll")
	GetDiskFreeSpaceExW := kernel32.MustFindProc("GetDiskFreeSpaceExW")
	for _, disk := range []string{"C:", "D:", "E:", "F:"} {
		str16, _ := syscall.UTF16PtrFromString(disk)
		_, _, err := GetDiskFreeSpaceExW.Call(uintptr(unsafe.Pointer(str16)), uintptr(unsafe.Pointer(&freeBytes)))
		if err.Error() == "The operation completed successfully." {
			text += fmt.Sprintf("%s %s Гб ", disk, int64_to_Gb(freeBytes))
		}
	}
	text += "\n"

	//Читаем свободное ОЗУ
	mem, _ := mem.VirtualMemory()
	text += fmt.Sprintf("ОЗУ Всего: %s Гб Свободно: %s Гб\n", uint64_to_Gb(mem.Total), uint64_to_Gb(mem.Free))

	text += "CPU "
	var cpupercent float64
	ps, err := cpu.Percent(time.Second, true)
	if err == nil && len(ps) > 0 {
		for _, val_cpu := range ps {
			cpupercent += val_cpu
			text += fmt.Sprintf("%.1f%% ", val_cpu)
		}
		cpupercent /= float64(len(ps))
	}
	text += fmt.Sprintf("Cреднее %.1f%%\n", cpupercent)

	//Время запуска ОС
	time_os2, _ := host.BootTime()
	text += fmt.Sprintf("Время запуска ОС: %v", time.Unix(int64(time_os2), 0))

	telegramm.Bot_send_message(user_SkorPay.User_bot.ID, user_SkorPay.User_bot, text, 0)
}

func int64_to_Gb(count_bytes int64) string {
	return fmt.Sprintf("%.1f", float32(count_bytes)/1073741824)
}

func uint64_to_Gb(count_bytes uint64) string {
	return fmt.Sprintf("%.1f", float32(count_bytes)/1073741824)
}

func otchet_dolgov(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	send_cmd_to_SkorPay_and_await_answer(user_SkorPay.User_bot, "otchet_dolgov", cmd, MessageID)
}

func notification(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	telegramm.Edit_notification(user_SkorPay.User_bot)
}

func about_bot(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	// заменяем в тексте вычисляемые значения типа {Telegtamm_use_webhook} на значения
	text := strings.ReplaceAll(telegramm.Param_str["message_text_for_about_bot"], "<br>", "\n")
	for name_param, value_param := range telegramm.Param_str {
		text = strings.Replace(text, "{"+name_param+"}", value_param, 1)
	}
	for name_param, value_param := range telegramm.Param_bool {
		text = strings.Replace(text, "{"+name_param+"}", fmt.Sprint(value_param), 1)
	}

	//Время exe файла
	text = strings.Replace(text, "{TimeExe}", telegramm.Vertion(), 1)

	telegramm.Bot_send_message(user_SkorPay.User_bot.ID, user_SkorPay.User_bot, text, 0)
}

// получение строкового значения из реестра по ключу name_key
func get_string_from_reestr(name_key string) string {
	rez, _, err := Keys_reestr[""].GetStringValue(name_key)
	if err == nil {
		return rez
	}
	log.Println("Ошибка чтения ветки ", name_key, err)
	return ""
}

/*

		# возвращает словарь инфо о карте
def info_about_card(karta)->dict: # собирает информацию о карте karta
    dict_info_card = {'_Баланс': '-', '_Баланс_Дата':'', '_Использовать_с': '', '_КолПлатежейВДень': 0, '_last_error_screenshot': '',
                      '_MaxCountPayInDay': '∞', '_min_summa':'', '_SummaRefillInDay':0, '_CountRefillInDay':0, '_MaxCountRefillInDay':''}

    # значения в sm.get_value_from_reestr_today
    list_key_reestr = ['_SummaRefillInDay', '_CountRefillInDay', '_КолПлатежейВДень']

    dict_info_bank = {'Groups_name':[]}

    # инфо по банку карты
    for key in dict_info_bank:
        dict_info_bank[key] = sm.ПолучитьЗначениеКлючаРеестра(karta[0:2] + key, dict_info_bank[key])

    # добавление ключей по группам банка
    if len(dict_info_bank['Groups_name']) > 0:
        for key in dict_info_bank['Groups_name']:
            _key = 'operators_' + key
            dict_info_card[_key] = sm.ПолучитьЗначениеКлючаРеестра(f'{karta}_{_key}', '')

    # получение всех значений из реестра по карте
    for key in dict_info_card:
        if key in list_key_reestr:
            dict_info_card[key] = sm.get_value_from_reestr_today(karta + key, dict_info_card[key])
        else:
            dict_info_card[key] = sm.ПолучитьЗначениеКлючаРеестра(karta + key, dict_info_card[key])

    dict_info_card['Groups_name'] = dict_info_bank['Groups_name']


    today = str(datetime.datetime.today())[:10]

    if dict_info_card['_Использовать_с'] < today:
        dict_info_card['_Использовать_с'] = ''
    count_pay_in_day, _last_error_screenshot = dict_info_card['_КолПлатежейВДень'], dict_info_card['_last_error_screenshot']
    # dict_info_card['_КолПлатежейВДень'] = count_pay_in_day[11:] if count_pay_in_day[0:10] == today else ''
    if _last_error_screenshot != '':
        # pos_year = dict_info_card['_last_error_screenshot'].find(today[0:4])
        if today in _last_error_screenshot:
            #     ошибка сегодня
            title_err = 'E'+_last_error_screenshot[-10:-6]
        else:
            title_err = 'e'
        dict_info_card['_last_error_screenshot'] = f'<a href="{_last_error_screenshot}">{title_err}</a>'
    return dict_info_card

Удалить
	//mapInfoCard := map[string]string{"_Баланс": "", "_Баланс_Дата": "", "_Использовать_с": "", "_КолПлатежейВДень": "", "_last_error_screenshot": "", "_МаксимальноеКолПлатежейВДень": "∞", "_min_summa": "-"}

*/
// возаращает map данных о карте card
func get_info_about_card(card string) (mapInfoCard map[string]string) {
	var key string
	mapInfoCard = map[string]string{}

	//список всех ключей реестра
	str_list_info := "_Баланс,_Баланс_Дата,_Использовать_с,_КолПлатежейВДень,_last_error_screenshot,_MaxCountPayInDay,_min_summa,_SummaRefillInDay,_CountRefillInDay,_MaxCountRefillInDay"

	//значения ключей, знаяения в ресстре для них в беретеся на текущий день
	list_keys_today := "_SummaRefillInDay,_CountRefillInDay,_КолПлатежейВДень"

	//ключи с Integer типом
	list_keys_int := "_МаксимальноеКолПлатежейВДень,_min_summa" // "∞"

	//группы операторов карты
	mapInfoCard["Groups_name"], _, _ = Keys_reestr[""].GetStringValue(card[0:2] + "Groups_name")

	// операторы в группах банка
	len_Groups_name := len(mapInfoCard["Groups_name"])
	if len_Groups_name > 0 {
		for _, key = range strings.Split(mapInfoCard["Groups_name"][1:len_Groups_name-1], ",") {
			mapInfoCard[key], _, _ = Keys_reestr[""].GetStringValue(card + "_" + key)
		}
	}

	// строку со списком path в mapInfoCard
	for _, key = range strings.Split(str_list_info, ",") {

		if strings.Contains(list_keys_today, key) {
			//значение за сегодня
			mapInfoCard[key] = get_value_today(Keys_reestr[""], card+key)
		} else {
			if strings.Contains(list_keys_int, key) {
				// значение в реестре как строка
				mapInfoCard[key], _, _ = Keys_reestr[""].GetStringValue(card + key)
			} else {
				// значение в реестре как число
				valueInt, _, err := Keys_reestr[""].GetIntegerValue(card + key)
				if err == nil {
					mapInfoCard[key] = strconv.FormatUint(valueInt, 10)
				}
			}

		}

	}

	//очищение значений, если старая дата
	today := time.Now().Format("2006-01-02")
	if mapInfoCard["_Использовать_с"] < today {
		mapInfoCard["_Использовать_с"] = ""
	}
	countPayInDay, lastErrorScreenshot := mapInfoCard["_КолПлатежейВДень"], mapInfoCard["_last_error_screenshot"]
	if len(countPayInDay) >= 10 && countPayInDay[:10] == today {
		mapInfoCard["_КолПлатежейВДень"] = countPayInDay[11:]
	} else {
		mapInfoCard["_КолПлатежейВДень"] = ""
	}

	// для ошибка сегодня добавляем "E"
	len_str := len(lastErrorScreenshot)
	if len_str > 0 {
		var titleError string
		if strings.Contains(lastErrorScreenshot, today) {
			// ошибка сегодня
			titleError = "E" + lastErrorScreenshot[len_str-10:len_str-6]
		}
		mapInfoCard["_last_error_screenshot"] = titleError
		//mapInfoCard["_last_error_screenshot"] = `<a href=` + lastErrorScreenshot + `>` + titleError + `</a>`
	}
	return mapInfoCard
}

// возвращает значение сохраненнное ранее за текущий день
// пример значения в реестре "2021-12-13:5000"
func get_value_today(key_reestr registry.Key, path string) string {
	value, _, err := key_reestr.GetStringValue(path)
	if err != nil || value[0:10] != time.Now().Format("2006-01-02") {
		return ""
	}
	return value[10:]
}

//поиск элемента в массиве
func find_string(what string, where []string) (idx int) {
	for i, v := range where {
		if v == what {
			return i
		}
	}
	return -1
}

func CheckFatallError(err error, args ...interface{}) {
	if err != nil {
		log.Fatal(err, args)
	}
}

func Check_err(err error, args ...interface{}) {
	if err != nil {
		log.Println(err, args)
	}
}

// обработчик команды /change_bit_*
//в *cmd после "change_bit_" номер уведомления
func change_bit_check_box(user_SkorPay *User_SkorPay, name_cmd, cmd *string, MessageID int) {
	if user_SkorPay.User_bot.Cmd_Сhange_bit_check_box(cmd) {
		notification(user_SkorPay, name_cmd, cmd, MessageID)
		user_SkorPay.Save_to_db()
	}
}
