/*
Чат бот телеграмм для управления программой проведения платежей SkorPay
При параметре stop завершает работу всех экземпляров программы
*/

package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	tgbotapi "github.com/go-telegram-Bot-api/telegram-Bot-api"
	"github.com/golang/glog"
	"github.com/xlab/closer"
	"main.go/telegramm"
)

var updates_bot tgbotapi.UpdatesChannel

func main() {
	//логирование на экран и в файл
	fileLog := prepare_log("SkorPay_telegram_bot.log")
	defer fileLog.Close()

	defer func() {
		log.Println("recover err")
		if err := recover(); err != nil {
			log.Printf("Recovered from err: %v\n", err)
			//log.Flags()
			glog.Flush()
		}
	}()

	// Обработка параметров
	if len(os.Args) > 1 {
		if strings.ToLower(os.Args[1]) == "stop" {
			fmt.Println("Заверщение работы всех процессов.")
			err := exec.Command("TASKKILL.exe", "/IM", filepath.Base(os.Args[0]), "/T", "/F").Start()
			log.Fatalln("err=", err)
		}
	}

	//закрытие объектов по завршению работы
	defer closer.Close()
	closer.Bind(cmd_bot_Close)

	//Инициализация канла updates_bot для сообщений от чат бот Телеграмм
	updates_bot = telegramm.Init_bot()

	//Инициализация модуля cmd_bot
	init_cmd_bot()

	//info, _ := host.Info()
	//telegramm.Bot_send_message_admins("Начало работы бота на сервере " + info.Hostname + " " + telegramm.Vertion())

	//бесконечный цикл чтения команд из канала updates_bot
	var cmd string
	for update := range updates_bot {

		// Пропускаем не понятные входящие сообщения
		if update.Message == nil {
			continue
		}

		//определение пользователя
		User_ID := telegramm.Type_user_ID(update.Message.From.ID)
		User_ID_map := fmt.Sprint(User_ID)
		user_SkorPay, ok := Users_SkorPay[User_ID_map]
		if !ok {
			//новый пользователь телеграмм
			User_bot := &telegramm.User_bot{ID: User_ID, Name: update.Message.From.FirstName, Nikname: update.Message.From.UserName}
			//, last_item_menu_bot: telegramm.Menu_bot.Childs["Основные"]
			telegramm.Users_bot[User_ID] = User_bot

			//новый пользователь SkorPay
			user_SkorPay = &User_SkorPay{User_bot: User_bot}
			Users_SkorPay[User_ID_map] = user_SkorPay

			//права
			if telegramm.Arr_string_contains(telegramm.Param_strings["users_list"], update.Message.From.UserName) {
				User_bot.Rights = 2 // Права на группу "Пользователь" бит 2
			} else {
				// нет прав

				//сообщение пользователю
				msg := tgbotapi.NewMessage(int64(User_ID), telegramm.Param_str["message_text_for_new_user"])
				msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
				telegramm.Bot.Send(msg)

				//собщение админам
				telegramm.Bot_send_message_admins(fmt.Sprintf("Новый пользователь @%s %s %s %d"+update.Message.From.UserName, update.Message.From.FirstName, update.Message.From.LastName, update.Message.From.ID))
				continue
			}
		} else if user_SkorPay.User_bot.Rights == 0 {
			//Пользователь без прав
			telegramm.Bot_send_message_admins(fmt.Sprintf("Сообщение от пользователя без прав @%s %d: %s", update.Message.From.UserName, update.Message.From.ID, update.Message.Text))
			continue
		}

		log.Println("New message", user_SkorPay.User_bot.Nikname, update.Message.Text)

		//Если есть канал, то введенное значение отправляем в канал
		if user_SkorPay.User_bot.Input_chan != nil {
			go func() {
				*user_SkorPay.User_bot.Input_chan <- update.Message.Text
			}()
			continue
		}

		//по тексту сообщения получаем name команды
		name_cmd := update.Message.Text
		if name_cmd[0:1] == "/" {
			// меняем местами cmd и name_cmd
			//cmd, name_cmd = name_cmd[1:], telegramm.Cmd_name[name_cmd]
			cmd, name_cmd = name_cmd, telegramm.Cmd_name[name_cmd]
		} else {
			cmd = telegramm.Name_to_cmd(name_cmd) //[1:]
		}

		//по name команды получаем фунцию и выполняем её
		func_cmd, ok := Name_to_func[name_cmd]
		if ok {
			telegramm.Get_new_Markup(user_SkorPay.User_bot, &name_cmd)
			go func_cmd(user_SkorPay, &name_cmd, &cmd, update.Message.MessageID)
		} else {
			telegramm.Bot_send_message(telegramm.Type_user_ID(update.Message.Chat.ID), user_SkorPay.User_bot, "Не известная команда "+update.Message.Text, 0)
		}

	}
}

//подготовка к логированию на экран и в файл
func prepare_log(name_log_file string) *os.File {
	//name_log_file := filepath.Base(os.Args[0]) + ".log"
	f, err := os.OpenFile(name_log_file, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}

	wrt := io.MultiWriter(os.Stdout, f)
	log.SetOutput(wrt)

	log.Println("Info Начало работы лога. ", name_log_file)

	return f
}
