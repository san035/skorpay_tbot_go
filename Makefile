include .env
export
PROJECTNAME=SkorPay_telegram_bot.exe

build:
	go build -o $(PROJECTNAME)

stop:
	echo остановка всех процессов $(PROJECTNAME)
	$(PROJECTNAME) stop
	
start:
	$(PROJECTNAME)

run:
	go run main.go cmd_bot.go

restart: stop start

env:
	set