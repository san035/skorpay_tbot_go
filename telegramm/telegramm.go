//Модуль для работы с API Телеграмм
package telegramm

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-git/go-git/v5"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/davecgh/go-spew/spew"
	tgbotapi "github.com/go-telegram-Bot-api/telegram-Bot-api"
	"github.com/xlab/closer"
	"gopkg.in/ini.v1"
)

type User_bot struct {
	ID                 Type_user_ID `json:"ID"`
	Name, Nikname      string
	Rights             uint16    `json:"Rights"`
	LastTime           time.Time // время роследнего доступа
	last_item_menu_bot *Menu_bot_item
	Input_chan         *chan string // ссылка на канал ввода
	Check_box          uint16       `json:"Check_box"`
}

var Users_bot = map[Type_user_ID]*User_bot{} //Все пользователи бота

//Меню бота в ini файле
type type_Menu_bot_ini map[string]type_Menu_bot_ini

//Меню бота в памяти
type Map_childs map[string]*Menu_bot_item
type Menu_bot_item struct {
	Parent         *Menu_bot_item
	Cmd            string     //`json:"Cmd"`
	Name           string     //`json:"Name"`
	Params         string     //`json:"Params"`
	Rights         uint16     //`json:"Rights"`
	Childs         Map_childs //`json:"Childs"`
	KeyboardButton tgbotapi.ReplyKeyboardMarkup
}

var Menu_bot_ini = type_Menu_bot_ini{}
var Menu_bot = Menu_bot_item{Parent: nil}
var Bot *tgbotapi.BotAPI
var err error

type Type_user_ID int              //int64
var Cmd_name = map[string]string{} // соответсвие команды и названия команды, пример Cmd_name["/users"] == "Пользователи"

var cfg *ini.File
var Param_str = map[string]string{}
var Param_bool = map[string]bool{}
var Param_int = map[string]int{}
var Param_ints = map[string][]int{}       // массив int
var Param_strings = map[string][]string{} // массив строк

var Нome_dir string // дамашний каталог

//Исправление ошибки MarshalJSON
// Type_user_ID сохранялась как строка
func (value Type_user_ID) MarshalJSON() ([]byte, error) {
	json, err := json.Marshal(int(value))
	return json, err
}

func (cf *Type_user_ID) UnmarshalJSON(data []byte) error {
	var err error
	if data[0] == 34 {
		err = json.Unmarshal(data[1:len(data)-1], &cf)
	} else {
		err = json.Unmarshal(data, &cf)
	}
	if err != nil {
		return errors.New("Type_user_ID: UnmarshalJSON: " + err.Error())
	}
	return nil
}

//возвращает канал сообщений
func Init_bot() (updates tgbotapi.UpdatesChannel) {
	var err error

	// дамашний каталог
	Нome_dir, _ = os.Getwd()
	//Нome_dir = filepath.Dir(os.Args[0])
	log.Println("Нome_dir =", Нome_dir)

	// Читаем config.ini из каклога exe файла
	cfg, err = ini.LoadSources(ini.LoadOptions{
		UnescapeValueCommentSymbols: true,
		UnparseableSections:         []string{"info"},
		AllowPythonMultilineValues:  true},
		"config.ini")
	CheckFatallError(err)

	//загружаем config.ini[telegramm]*
	Load_all_params_from_ini("telegramm", "admin_users_list,users_list,ignor_users_list,rights_users,names_check_box", []string{})     // значения - список строк
	Load_all_params_from_ini("telegramm", "keys_in_env,message_text_for_new_user,message_text_for_about_bot,ParseMode,URL_githab", "") // значения - строки
	Load_all_params_from_ini("telegramm", Param_str["keys_in_env"], "")                                                                // значения - строки
	Load_all_params_from_ini("telegramm", "Debug", false)                                                                              // значения - bool
	Load_all_params_from_ini("telegramm", "max_columns_menu", 0)                                                                       // значения - int

	Load_bot_menu()

	//загрузка настроек
	Load_env(Param_str["keys_in_env"])

	if Param_bool["Debug"] {
		spew.Dump(Param_str)
	}

	//запуск бота
	Bot, err = tgbotapi.NewBotAPI(Param_str["Telegtamm_token_Skorpay_bot"])
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Authorized on account %s", Bot.Self.UserName)
	Bot.Debug = Param_bool["Debug"]

	if Param_str["Telegtamm_use_webhook"] == "1" {
		//работаем через webhook
		Param_str["Telegtamm_webhook_crt"] = "cert\\server.crt"
		Param_str["Telegtamm_webhook_key"] = "cert\\server.key"
		url_webhook := Param_str["Telegtamm_webhook_url"] + ":" + Param_str["Telegtamm_webhook_port"] + "/" + Bot.Token
		log.Println("Настройка webhook:" + url_webhook)

		info, err := Bot.GetWebhookInfo()
		if err != nil {
			panic(err)
		}

		if !info.IsSet() {
			wh := tgbotapi.NewWebhookWithCert(url_webhook, Param_str["Telegtamm_webhook_crt"]) // tgbotapi.FilePath(
			_, err = Bot.SetWebhook(wh)
			if err != nil {
				panic(err)
			}
			info, err = Bot.GetWebhookInfo()
			if err != nil {
				panic(err)
			}
		}
		if info.LastErrorDate != 0 {
			log.Printf("failed to set webhook: %s", info.LastErrorMessage)
		}

		updates = Bot.ListenForWebhook("/" + Bot.Token)
		go http.ListenAndServeTLS("0.0.0.0:88", Param_str["Telegtamm_webhook_crt"], Param_str["Telegtamm_webhook_key"], nil)
	} else {
		//Выключаем webhook
		info, _ := Bot.GetWebhookInfo()
		if info.IsSet() {
			resp, err := Bot.MakeRequest("deleteWebhook", nil)
			log.Println("Удаление webhook:", resp, err)
		}

		u := tgbotapi.NewUpdate(0)
		u.Timeout = 60
		updates, _ = Bot.GetUpdatesChan(u)
	}

	return updates
}

//загрузка бот меню из config.ini
func Load_bot_menu() {

	//Загружаем меню бота в строку
	bot_menu_str := cfg.Section("telegramm").Key("bot_menu").String()
	bot_menu_str = strings.Replace(bot_menu_str, `""`, "null", -1)

	//конвертирруем строку в объект Menu_bot_ini
	err = json.Unmarshal([]byte(bot_menu_str), &Menu_bot_ini)
	CheckFatallError(err)
	//log.Println(Menu_bot_ini)

	//Menu_bot_ini -> Menu_bot
	move_map_ini_items_menu_to_items_menu(&Menu_bot, &Menu_bot_ini)

}

//рекурсивный перенос меню  из формата ini (menu_ini) в рабочее меню (menu_expand)
func move_map_ini_items_menu_to_items_menu(menu_expand *Menu_bot_item, menu_ini *type_Menu_bot_ini) {
	if menu_ini != nil && len(*menu_ini) > 0 {
		menu_expand.Childs = Map_childs{}
	}

	menu_expand.KeyboardButton = tgbotapi.ReplyKeyboardMarkup{
		ResizeKeyboard: true,
		Keyboard:       [][]tgbotapi.KeyboardButton{{}},
	}

	// обход по menu_ini
	num_menu := 0
	for key, childs_menu_ini := range *menu_ini {

		//key раскладываем "Cmd - Name - Params" в массив arr_key
		count_sep := strings.Count(key, "-")
		if count_sep == 0 {
			// нет разделителя, все значение это Name
			key = "-" + key + "-"
		} else if count_sep == 1 {
			key += "-"
		}
		arr_key := strings.Split(key, "-")

		//убираем пробелы
		for key, val := range arr_key {
			arr_key[key] = strings.TrimSpace(val)
		}

		//если есть команда бот, то сохраняем соответствие команды и названия
		if arr_key[0] != "" {
			Cmd_name[arr_key[0]] = arr_key[1]
		}

		new_item_menu_expand := &Menu_bot_item{Cmd: arr_key[0], Name: arr_key[1], Params: arr_key[2], Parent: menu_expand}
		if childs_menu_ini != nil {
			move_map_ini_items_menu_to_items_menu(new_item_menu_expand, &childs_menu_ini)
		}

		//заполнение KeyboardButton - это создание кнопок в формате Телеграмм
		num_str_menu := int(num_menu / Param_int["max_columns_menu"])
		num_menu++

		//добавляем новую строку меню
		if num_str_menu+1 > len(menu_expand.KeyboardButton.Keyboard) {
			menu_expand.KeyboardButton.Keyboard = append(menu_expand.KeyboardButton.Keyboard, []tgbotapi.KeyboardButton{})
		}

		//добавляем новую элемент меню
		menu_expand.KeyboardButton.Keyboard[num_str_menu] = append(menu_expand.KeyboardButton.Keyboard[num_str_menu], tgbotapi.NewKeyboardButton(new_item_menu_expand.Name))

		menu_expand.Childs[new_item_menu_expand.Name] = new_item_menu_expand
	}
}

func CheckFatallError(err error, args ...interface{}) {
	if err != nil {
		log.Fatal(err, args)
	}
}

func Check_err(err error, args ...interface{}) {
	if err != nil {
		log.Println(err, args)
	}
}

// Contains указывает, содержится ли x в a.
func Arr_string_contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

// пользователю (update.Message.From) возвращает значение типа User_bot
/*
func Get_user_bot(user_bot *tgbotapi.User) (user User_extended) {
	if user_old, ok := Users_bot[user_bot.ID]; ok {
		//пользователь уже есть в нашем map
		user = user_old
		return
	}

	//Новый пользователь
	Menu_bot_item := Menu_bot.Childs["Основные"]
	//user.ID = user_bot.ID
	user.Fill(user_bot.ID, user_bot.UserName, user_bot.FirstName, &Menu_bot_item)
	Users_bot[user_bot.ID] = user

	//проверим, может он админ
	if Arr_string_contains(Param_strings["admin_users_list"], user_bot.UserName) {
		user.SetRight(1) // Права на группу Админстратор
	} else {
		//Новый пользователь без прав
		msg := tgbotapi.NewMessage(user_bot.ID, Param_str["message_text_for_new_user"])
		msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
		Bot.Send(msg)
	}

	return
}
*/

//возвращает новое меню пользователя для команды name_cmd *tgbotapi.ReplyKeyboardMarkup
func Get_new_Markup(user *User_bot, name_cmd *string) {
	var ok bool
	user.LastTime = time.Now()
	if user.Rights == 0 {
		//Удаляем меню
		return
	}

	if user.last_item_menu_bot == nil {
		user.last_item_menu_bot = Menu_bot.Childs["Основные"]
	}

	if *name_cmd == "Return" {
		//На Уровень вверх
		ok = user.last_item_menu_bot.Parent != nil && user.last_item_menu_bot.Parent.Name != ""
		if ok {
			user.last_item_menu_bot = user.last_item_menu_bot.Parent
		}
	} else if user.last_item_menu_bot.Name == *name_cmd {
		// Меню уже выбрано
		return
	} else {
		//ok = user.last_item_menu_bot != nil
		//if ok {
		user.last_item_menu_bot, ok = user.last_item_menu_bot.Childs[*name_cmd]
		ok = ok && user.last_item_menu_bot != nil && user.last_item_menu_bot.Childs != nil
		//}
	}

	if !ok {
		user.last_item_menu_bot = Menu_bot.Childs["Основные"]
	}
}

//Отправка сообщения text только администраторам
func Bot_send_message_admins(text string) {
	for _, user_admin := range Param_strings["admin_users_list"] {
		if chat_ID_admin_int, err := strconv.Atoi(user_admin); err == nil {
			//Указан chat_ID
			Bot_send_message(Type_user_ID(chat_ID_admin_int), nil, text, 0)
		} else {

			//Ищем chat_ID в Users_bot
			user_admin = string(user_admin)
			//Ищем чат ID для пользователя user_admin
			for chat_ID_admin, user_bot := range Users_bot {
				if user_bot.Nikname == user_admin || fmt.Sprint(user_bot.ID) == user_admin {
					Bot_send_message(chat_ID_admin, nil, text, 0)
					break // переход к следующему админу
				}
			}
		}
	}
}

//Отправка сообщения text в чат chat_ID
//	ReplyMarkup:
//		или tgbotapi.ReplyKeyboardMarkup, tgbotapi.ReplyKeyboardRemove
//		или Menu_bot_item
//		или User_bot
func Bot_send_message(chat_ID Type_user_ID, ReplyMarkup interface{}, text string, ReplyToMessageID int) {
	var msg tgbotapi.MessageConfig
	log.Println("Message to chat", chat_ID, text)
	msg.BaseChat.ChatID, msg.Text = int64(chat_ID), text
	msg.ParseMode = Param_str["ParseMode"]

	//приведение типа интерфес к другомк типу
	switch ReplyMarkup.(type) {
	case *User_bot:
		last_item_menu_bot := ReplyMarkup.(*User_bot).last_item_menu_bot
		if last_item_menu_bot != nil {
			msg.ReplyMarkup = last_item_menu_bot.KeyboardButton
		}
	case tgbotapi.ReplyKeyboardMarkup, tgbotapi.ReplyKeyboardRemove:
		msg.ReplyMarkup = ReplyMarkup
	case Menu_bot_item:
		msg.ReplyMarkup = ReplyMarkup.(Menu_bot_item).KeyboardButton
		//case User_bot:
		//	msg.ReplyMarkup = ReplyMarkup.(User_bot).last_item_menu_bot.KeyboardButton
	}

	msg.ReplyToMessageID = ReplyToMessageID
	Bot.Send(msg)
}

//По названию команды возвращает команду
func Name_to_cmd(name string) string {
	for cmd, val := range Cmd_name {
		if val == name {
			return cmd
		}
	}
	return "-"
}

//загружаем все строковые параметры из ini
// str_list_param_ini - список параметров через запятую
// type_param - пустое значение загружаемого параметре, по нему определяется тип
func Load_all_params_from_ini(section_ini, str_list_param_ini string, type_param interface{}) {
	var err error
	for _, key_ini := range strings.Split(str_list_param_ini, ",") {
		// определение типа переменной
		switch type_param.(type) {
		case string:
			if key_ini == "" {
				continue
			}
			var new_value string
			valueKey := cfg.Section(section_ini).Key(key_ini)
			if valueKey != nil {
				new_value = valueKey.String()
			}
			Param_str[key_ini] = new_value
			if new_value == "" {
				log.Printf("Не найдено значение в config.ini [%s]%s\n", section_ini, key_ini)
			}
		case bool:
			Param_bool[key_ini], err = cfg.Section(section_ini).Key(key_ini).Bool()
			if err != nil {
				log.Println(err.Error())
			}
		case []string:
			Param_strings[key_ini] = cfg.Section(section_ini).Key(key_ini).Strings(",")
		case int:
			Param_int[key_ini], _ = cfg.Section(section_ini).Key(key_ini).Int()
		case []int:
			Param_ints[key_ini] = cfg.Section(section_ini).Key(key_ini).Ints(",")
		}
	}
}

// загрузка параметров в Param_str, загружаются только не пустые значения
// params_env - список парматров через запятую
func Load_env(params_env string) {
	if params_env == "" {
		return
	}

	for _, param := range strings.Split(params_env, ",") {
		param = strings.TrimSpace(param)
		value_param := os.Getenv(param)
		if value_param != "" && Param_str[param] == "" {
			Param_str[param] = value_param
			log.Printf("Значение для ключа %s из ini файла заменено из переменных окружения на %s.", param, value_param)
		}

	}

	return
}

//полученние суммы введенной в чат боте
func Input_summa(user_bot *User_bot, comment_input string) (summa string) {
	summa = Input_string(user_bot, comment_input)

	//проверка введенного значения на число
	_, err := strconv.ParseFloat(summa, 32)
	if err != nil {
		Bot_send_message(user_bot.ID, nil, "Введенное значение не число, ошибка "+err.Error(), 0)
		return ""
	}
	return
}

//функция возвращает строку введенной в чат боте
func Input_string(user_bot *User_bot, comment_input string) (input_value string) {
	Bot_send_message(user_bot.ID, nil, comment_input, 0)

	//создаем канал, получаем из него сумму, закрываем канал
	Input_chan := make(chan string, 1)
	user_bot.Input_chan = &Input_chan
	input_value = <-Input_chan
	close(Input_chan)
	user_bot.Input_chan = nil

	return
}

//Обновление программы
func Update_programm(chat_ID Type_user_ID) {
	name_exe := filepath.Base(os.Args[0])
	if strings.Contains(name_exe, "debug") {
		Bot_send_message(chat_ID, nil, "Ошибка обновления. Запуск из отладчика "+os.Args[0], 0)
		//return
	}

	var err error
	os.Chdir(Нome_dir) // Восстанавливаем домашнюю папку

	new_build_file := "update\\" + name_exe

	if _, err = os.Stat(new_build_file); err == nil {
		Bot_send_message(chat_ID, nil, "Уже есть новый файл "+new_build_file, 0)
		Reboot_bot(chat_ID)
		return
	}

	//Нет нового exe, создаем его
	err = os.RemoveAll("update\\")
	log.Println("err RemoveAll=", err)

	Bot_send_message(chat_ID, nil, "Обновление программы /update\nШаг 1 скачивание обновлений с "+Param_str["URL_githab"], 0)
	_, err = git.PlainClone("update", false, &git.CloneOptions{URL: Param_str["URL_githab"], Progress: os.Stdout})
	if err != nil {
		Bot_send_message(chat_ID, nil, "Ошибка "+fmt.Sprint(err), 0)
		return
	}

	//Запускаем компиляцию если нет exe файла
	if _, err = os.Stat(new_build_file); err == nil {
		Bot_send_message(chat_ID, nil, "Шаг 2 Компиляция - пропускается т к исполняемый файл уже создан "+new_build_file, 0)
	} else {
		//file_build := Нome_dir + "\\update\\build.cmd"
		file_build := Нome_dir + "\\update\\make.exe"
		Bot_send_message(chat_ID, nil, "Шаг 2 Компиляция "+file_build+" build", 0)

		os.Chdir(Нome_dir + "\\update") // текущая папка update

		//err = exec.Command("cmd.exe", "/C", "start", "/Wait", "/low", file_build, "build").Run()
		err = exec.Command(file_build, "build").Run()
		log.Println("build.cmd:", err)
		if err != nil {
			Bot_send_message(chat_ID, nil, "Ошибка build.cmd: "+fmt.Sprint(err), 0)
			return
		}
		os.Chdir(Нome_dir) //Возвращаем текущий каталог

		//Проверяем наличие файла
		if _, err = os.Stat(new_build_file); err != nil {
			Bot_send_message(chat_ID, nil, "После компиляции не найден исполняемый файл "+new_build_file, 0)
			return
		}

	}

	//Bot_send_message(chat_ID, nil, "Перезапуск бота: /reboot_bot", 0)
	Reboot_bot(chat_ID)
}

//Перезапуск программы
func Reboot_bot(chat_ID Type_user_ID) {
	Bot_send_message(chat_ID, nil, "Перезапуск бота /reboot_bot", 0)

	os.Chdir(Нome_dir) // Восстанавливаем домашнюю папку

	file_restart := Нome_dir + "\\restart.cmd"
	log.Println("Запуск", file_restart)
	err := exec.Command("cmd.exe", "/C", "start", "/low", file_restart, filepath.Base(os.Args[0])).Run()
	log.Println("err запуска", err)
	if err == nil {
		log.Println("Завершение работы для перезапуска ПО", file_restart)
		closer.Close()
	}

	Bot_send_message(chat_ID, nil, "Ошибка перезапуска:"+err.Error(), 0)
}

//Возаращает версию чат бота - Время exe файла
func Vertion() string {
	fi, _ := os.Stat(os.Args[0])
	return fi.ModTime().Format("02.01.2006 15.04.05 MST")
}

//Редактирование уведомлений
func Edit_notification(user_bot *User_bot) {
	text := "Настройка уведомлений /notification\n" //<pre>
	for id_noti, name_noti := range Param_strings["names_check_box"] {
		value_check := user_bot.Get_value_check_box_by_id(id_noti)
		text += fmt.Sprintf("%t %s /change_bit_%d\n", value_check, name_noti, id_noti)
	}
	//text += "</pre>"

	Bot_send_message(user_bot.ID, nil, text, 0)
}

//возвращает занчение бита с номером id_noti для пользователя
//id_noti - это номер бита в user_bot.Check_box от 0 до 15
func (user_bot *User_bot) Get_value_check_box_by_id(id_noti int) bool {
	return (user_bot.Check_box >> id_noti & 1) == 1
}

//Установка бита id_noti в новое инвертированное значение
func (user_bot *User_bot) Change_check_box_by_id(id_noti uint16) {
	// ^ - это xor, т e побитно: 1001 & 0011 = 1010
	//1 << n это сдвиг 1 на n разрядов, пример 0001 << 2 = 0100
	user_bot.Check_box = user_bot.Check_box ^ (1 << id_noti)
}

//обработчик команды /change_bit_*
//возвращает признак успешности изменения user_bot.Check_box
func (user_bot *User_bot) Cmd_Сhange_bit_check_box(cmd *string) bool {
	//в *cmd после "change_bit_" номер уведомления
	num_bit, err := strconv.ParseUint((*cmd)[11:], 10, 0)
	if err != nil {
		Bot_send_message(user_bot.ID, nil, "Ошибочный номер в команде "+*cmd, 0)
		return false
	}
	user_bot.Change_check_box_by_id(uint16(num_bit))
	return true
}
